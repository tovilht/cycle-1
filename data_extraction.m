size = input('Please input DNA size? ');
start_point = input('Please input starting point? ');
end_point = input('please input end point? ');

for i = 1:5
    file_name = ['data\' int2str(size) 'kb\100pM' int2str(size) 'kb_300mV_' int2str(i) '_Chan_1_workspace.mat'];
    file = load(file_name);

    dwell_time = file.Width;
    peak = file.PkMaxBurst;
    charge = file.Area;

    T = table(dwell_time, peak, charge);

    writetable(T, [int2str(size) 'kb_' int2str(i) '.csv']);
end