# Cycle 1

MSci Project 12/10 - 23/10

## 29/10/2020 update

1. follow instructions on <https://github.com/mathworks-ref-arch/matlab-on-aws> and <https://github.com/mathworks-ref-arch/matlab-on-aws/blob/master/releases/R2020b/README.md> to set up the AWS

login: ubuntu

2. in the nanopore app, rmb to save workspace
3. load data in matlab:
   `file = load('filename.mat)`
4. to read the variables:
   `file.app`

## 02/11/2020 update

1. fitting curve -> LingFit
2. dwell time -> Width
3. peak amplitude -> PkMaxBurst
4. charge -> Area

### code for automatic data extraction from Matlab workspace

1. The folder containing the files should be named `data`
2. in `data_extraction.m`:

```
size = input('Please input DNA size? ');
start_point = input('Please input starting point? ');
end_point = input('please input end point? ');

for i = 1:5
    file_name = ['data\' int2str(size) 'kb\100pM' int2str(size) 'kb_300mV_' int2str(i) '_Chan_1_workspace.mat'];
    file = load(file_name);

    dwell_time = file.Width;
    peak = file.PkMaxBurst;
    charge = file.Area;

    T = table(dwell_time, peak, charge);

    writetable(T, [int2str(size) 'kb_' int2str(i) '.csv']);
end
```

This will create the appropriate csv files for further analysis in `analysed_data` folder

**please note that the inputs only accept integers**

## 04/11/2020 update

### data concatenation with Python

1. in jupyter notebook:

```python
%pylab inline
import pandas as pd
import random
import os
df = pd.DataFrame({'dwell_time': [], 'peak': [], 'charge': [], 'size': []})
for each_file in os.listdir("."):
    if each_file.endswith(".csv"):
        size = int(each_file.split('k')[0])
        file = pd.read_csv(each_file)
        file['size'] = linspace(size,size,len(file))
        df = df.append(file)
df.to_csv('merged.csv')
```

this will concatenate all .csv files in the current directory exported by Matlab into one csv file `analysed_data\merged_1.csv`

2. data concatenation was done in python since I'm more familiar with the language with handier libraries.

### training with Matlab Classification Learner (3 features: dwell time, peak amplitude and charge)

1. create new session with the concatenated csv `analysed_data\merged.csv` imported
2. all classification methods were ran with default settings:
   1. ![all_methods_1](/training/All models 1-12.png)
   2. ![all_methods_2](/training/All models 13-24.png)
3. similar results were obtained using various models: 86% accuracy
4. using Optimizable Tree with the following settings: ![settings](/training/Optimizable Tree Settings.PNG)
5. the results were obtained as follows:

   1. ![scatter time vs charge](/training/Optimizable Tree scatter plot time_charge.png)
   2. ![scatter time vs peak](/training/Optimizable Tree scatter plot time_peak.png)
   3. ![confusion_matrix](/training/Optimizable Tree confusion matrix.png)
   4. ![minimum_error](/training/Optimizable Tree minimum classifcation error plot.png)
   5. ![ROC](/training/Optimizable Tree ROC curve.png)

### Results & Discussion

1. with 3 features investigated, the average optimal accuracy is around **86%** for a classifcation learner, regardless of the model chosen
2. the accuracy in descending order: 5kb (94.7%) > 48.5kb (75.7%) > 20kb (66.0%)
3. the three sizes of sample overlap with each other, with the scattering diagram showing the that an increase in size will increase dispersion
4. 20 kb stands in between 5 kb and 48.5 kb, leading to a low detection accuracy

###
